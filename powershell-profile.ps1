## To get 'Az' download Azure-cli
## "Choco install azure-cli kubernetes-helm flux"

## Delete word by word 
Set-PSReadLineKeyHandler -Chord ctrl+w -Function BackwardDeleteWord

## Load Terminal-Icons
# First install: "Install-Module -Name Terminal-Icons -Repository PSGallery"
Import-Module -Name Terminal-Icons

# Custom aliases
Set-Alias -Name k -Value kubectl
set-alias -Name ss -Value select-string

## kubernetes autocomplete
#manual install first: "Install-Module -Name PSKubeContext -RequiredVersion 0.0.3"
Import-Module PSKubeContext  
Set-Alias kns -Value Select-KubeNamespace  
Set-Alias kubectx -Value Select-KubeContext  
Register-PSKubeContextComplete  

# Custom functions

# Change context to go to PROD-cluster
function clusterprod {
    az account set --subscription b3be650f-dd8c-4c29-b616-2ba19529e977
    az aks get-credentials --resource-group rg-k8sapps-prod-001 --name aks-k8sapps-prod-001 --overwrite-existing --admin
    write-host "You have now changed kubeconfig to go to PROD-cluster" -ForegroundColor Red
}

function clustertest {
    az account set --subscription b3be650f-dd8c-4c29-b616-2ba19529e977
    az aks get-credentials --resource-group rg-k8sapps-prod-001 --name aks-k8sapps-test-001 --overwrite-existing --admin
    write-host "You have now changed kubeconfig to go to TEST-cluster" -ForegroundColor Green
}

function clusterdanskebank {
    az account set --subscription b3be650f-dd8c-4c29-b616-2ba19529e977
    az aks get-credentials --resource-group rg-danskebank-test-002 --name aks-danskebank-test-002 --overwrite-existing --admin
    write-host "You have now changed kubeconfig to go to DANSKEBANK-cluster" -ForegroundColor Green
}

function clusterdev {
    az account set --subscription f1947fd7-c079-4eea-bebd-8d3dd89fd8db
    az aks get-credentials --resource-group Lighthouse_Collect_Dev --name lightkubedev --overwrite-existing --admin
    write-host "You have now changed kubeconfig to go to DEV-cluster" -ForegroundColor Green
}


## Show last 10 git commits on one line
function gitlog {
    git log $args[0] --oneline
}

## short commnand for listing installed helm chart
function hla {
    helm list -a $args
}

## short command for kubectl logs
function kl {
    kubectl logs $args
}

## short command for describing pod
function kdp {
    kubectl describe pod $args
}

## Get flux kustomization status
function fgk {
    flux get kustomization
}
## Get top nodes sorted by memory usage
function ktnmem {
    kubectl top node --sort-by='memory'
}

## Get top nodes sorted by cpu usage
function ktncpu {
    kubectl top node --sort-by='cpu'
}

## short command for listing pods in a namespace
function kgp {
    kubectl get pod
}

## short command for listing deployments in a namespace
function kgd {
    kubectl get deployments
}

## short command for listing services in a namespace
function kgs {
    kubectl get svc
}

## short command for listing ingresses in a namespace
function kgi {
    kubectl get ingress
}

## short command for editing a pod in a namespace
function kep {
    kubectl edit pod $args
}

## short command for editing a deployment in a namespace
function ked {
    kubectl edit deployments $args
}

## short command for editing a service in a namespace
function kes {
    kubectl edit svc $args
}

## short command for editing a ingress in a namespace
function kei {
    kubectl edit ingress $args
}

## short command "kubectl describe in a namespace"
function kd {
    kubectl describe $args
}

## Flux suspend and resume helm chart
function fluxretry {
    $hr = $args[1]
    $ns = $args[0]
    write-host "namespace: $ns"
    write-host "helm release name: $hr"
    write-host "This will run command: flux suspend hr $hr -n $ns; flux resume hr $hr -n $ns"
    timeout 5
    flux suspend hr $hr -n $ns; flux resume hr $hr -n $ns
}

## Search for given input
function find {
$searchname = Read-host "Enter keyword you want to search for"
Get-ChildItem $searchname -Recurse
}

## Go to root
function root {
    cd \
}

## Go to GitOps-aks-k8sapps-prod-001 root
function gitopsroot {
    cd c:\git\GitOps-aks-k8sapps-prod-001\
}

## Set non-existent kubernetes namepace
function idle {
    ns noNamespace
}

## Clear screen
function c {
    cls
}

##WORK IN PROGRESS - helm list -a --all-namespaces
function helmlistall {
    $date = Get-date | select Year, Month, Day
    helm list -a --all-namespaces --filter 'collect' | select-string $date   
}

## Create new AAD Security Group
function createaadgroup {
    $groupname = Read-host "Enter name of new AAD Security Group"
    $description = Read-host "Enter short and informative description if needed"
    New-AzADGroup -DisplayName $groupname -MailNickname $false -Description $description
}

# up&down arrow for history search
#Set-PSReadLineKeyHandler -Key UpArrow -Function HistorySearchBackward  
#Set-PSReadLineKeyHandler -Key DownArrow -Function HistorySearchForward 

# menu complete using TAB instead of CTRL+SPACE
Set-PSReadlineKeyHandler -Chord Tab -Function MenuComplete 

#Set default directory to most used git repository
# cd c:\git\GitOps-aks-k8sapps-prod-001
